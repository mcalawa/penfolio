﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("WritingProfile")]
    public partial class WritingProfile
    {
        [Key]
        [Required]
        public int WritingProfileID { get; set; }

        [Required]
        public int ProfileID { get; set; }

        [Required]
        public int WritingID { get; set; }

        public virtual PenProfile PenProfile { get; set; }

        public virtual Writing Writing { get; set; }
    }
}