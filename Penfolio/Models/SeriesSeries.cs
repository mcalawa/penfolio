﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("SeriesSeries")]
    public partial class SeriesSeries
    {
        [Key]
        [Required]
        public int SeriesSeriesID { get; set; }

        [Required]
        public int OverarchingSeriesID { get; set; }

        [Required]
        public int SeriesMemberID { get; set; }

        public int? PreviousSeriesID { get; set; }

        public int? NextSeriesID { get; set; }

        [Required]
        public bool IsStandAlone { get; set; }

        [ForeignKey("OverarchingSeriesID")]
        public virtual Series OverarchingSeries { get; set; }

        [ForeignKey("SeriesMemberID")]
        public virtual Series SeriesMember { get; set; }
    }
}