﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace Penfolio.Models
{

    [Table("Writing")]
    public partial class Writing
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Writing()
        {
            WritingProfiles = new HashSet<WritingProfile>();
            WritingFolders = new HashSet<WritingFolder>();
            WritingSeries = new HashSet<WritingSeries>();
            WritingFormats = new HashSet<WritingFormat>();
            WritingGenres = new HashSet<WritingGenre>();
            Comments = new HashSet<Comment>();
            Likes = new HashSet<Likes>();
            Critiques = new HashSet<Critique>();
            CritiqueRequest = new HashSet<CritiqueRequest>();
            NextSeriesWriting = new HashSet<WritingSeries>();
            PreviousSeriesWriting = new HashSet<WritingSeries>();
        }

        [Key]
        [Required]
        public int WritingID { get; set; }

        [Required]
        public int AccessPermissionID { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public byte[] Document { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy h:mm tt}")]
        public DateTime AddDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy h:mm tt}")]
        public DateTime? EditDate { get; set; }

        [Required]
        public bool LikesOn { get; set; }

        [Required]
        public bool CommentsOn { get; set; }

        [Required]
        public bool CritiqueOn { get; set; }

        [Required]
        public string DescriptionText { get; set; }

        [Required]
        public bool IsStandAlone { get; set; }

        [ForeignKey("Username")]
        public virtual PenUser PenUser { get; set; }

        public virtual AccessPermission AccessPermission { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WritingProfile> WritingProfiles { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WritingFolder> WritingFolders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WritingSeries> WritingSeries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WritingFormat> WritingFormats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WritingGenre> WritingGenres { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> Comments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Likes> Likes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Critique> Critiques { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CritiqueRequest> CritiqueRequest { get; set; }

        [ForeignKey("PreviousWritingID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WritingSeries> NextSeriesWriting { get; set; }

        [ForeignKey("NextWritingID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WritingSeries> PreviousSeriesWriting { get; set; }
    }
}
