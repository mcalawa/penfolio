﻿--Drop from least dependent up
IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'CritiqueGiver'
)
BEGIN
	DROP TABLE dbo.CritiqueGiver
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'FollowerFollowing'
)
BEGIN
	DROP TABLE dbo.FollowerFollowing
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'SeriesGenre'
)
BEGIN
	DROP TABLE dbo.SeriesGenre
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'FolderGenre'
)
BEGIN
	DROP TABLE dbo.FolderGenre
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'WritingGenre'
)
BEGIN
	DROP TABLE dbo.WritingGenre
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'GenreFormat'
)
BEGIN
	DROP TABLE dbo.GenreFormat
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'GenreCategory'
)
BEGIN
	DROP TABLE dbo.GenreCategory
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'AltGenreName'
)
BEGIN
	DROP TABLE dbo.AltGenreName
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'GenreTag'
)
BEGIN
	DROP TABLE dbo.GenreTag
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'SeriesFormat'
)
BEGIN
	DROP TABLE dbo.SeriesFormat
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'FolderFormat'
)
BEGIN
	DROP TABLE dbo.FolderFormat
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'WritingFormat'
)
BEGIN
	DROP TABLE dbo.WritingFormat
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'FormatCategory'
)
BEGIN
	DROP TABLE dbo.FormatCategory
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'AltFormatName'
)
BEGIN
	DROP TABLE dbo.AltFormatName
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'FormatTag'
)
BEGIN
	DROP TABLE dbo.FormatTag
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'CommentReply'
)
BEGIN
	DROP TABLE dbo.CommentReply
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'Likes'
)
BEGIN
	DROP TABLE dbo.Likes
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'CommentFlag'
)
BEGIN 
	DROP TABLE dbo.CommentFlag
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'Comment'
)
BEGIN
	DROP TABLE dbo.Comment
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'SeriesSeries'
)
BEGIN
	DROP TABLE dbo.SeriesSeries
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'WritingSeries'
)
BEGIN
	DROP TABLE dbo.WritingSeries
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'Series'
)
BEGIN
	DROP TABLE dbo.Series
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'FolderSubfolder'
)
BEGIN
	DROP TABLE dbo.FolderSubfolder
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'WritingFolder'
)
BEGIN
	DROP TABLE dbo.WritingFolder
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'Folder'
)
BEGIN
	DROP TABLE dbo.Folder
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'CritiqueRequest'
)
BEGIN 
	DROP TABLE dbo.CritiqueRequest
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'Critique'
)
BEGIN 
	DROP TABLE dbo.Critique
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'WritingProfile'
)
BEGIN
	DROP TABLE dbo.WritingProfile
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'Writing'
)
BEGIN
	DROP TABLE dbo.Writing
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'FriendRequest'
)
BEGIN
	DROP TABLE dbo.FriendRequest
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'Friendship'
)
BEGIN
	DROP TABLE dbo.Friendship
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'IndividualAccessRevoke'
)
BEGIN
	DROP TABLE dbo.IndividualAccessRevoke
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'IndividualAccessGrant'
)
BEGIN
	DROP TABLE dbo.IndividualAccessGrant
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'AccessRequest'
)
BEGIN
	DROP TABLE dbo.AccessRequest
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'UserBlock'
)
BEGIN
	DROP TABLE dbo.UserBlock
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'PenProfile'
)
BEGIN
	DROP TABLE dbo.PenProfile
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'PenRole'
)
BEGIN
	DROP TABLE dbo.PenRole
END

IF EXISTS
(
	SELECT *
	FROM sys.tables
	WHERE tables.name = 'AccessPermission'
)
BEGIN
	DROP TABLE dbo.AccessPermission
END