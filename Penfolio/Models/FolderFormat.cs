﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("FolderFormat")]
    public partial class FolderFormat
    {
        [Key]
        [Required]
        public int FolderFormatID { get; set; }

        [Required]
        public int FolderID { get; set; }

        [Required]
        public int FormatID { get; set; }

        public virtual Folder Folder { get; set; }

        public virtual FormatTag FormatTag { get; set; }
    }
}