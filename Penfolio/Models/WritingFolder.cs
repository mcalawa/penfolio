﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("WritingFolder")]
    public partial class WritingFolder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public WritingFolder()
        {
            FolderWritings = new HashSet<WritingFolder>();
        }

        [Key]
        [Required]
        public int WritingFolderID { get; set; }

        [Required]
        public int WritingID { get; set; }

        [Required]
        public int FolderID { get; set; }

        public virtual Writing Writing { get; set; }

        public virtual Folder Folder { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WritingFolder> FolderWritings { get; set; }
    }
}