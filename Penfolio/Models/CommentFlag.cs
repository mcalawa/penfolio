﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("CommentFlag")]
    public partial class CommentFlag
    {
        [Key]
        [Required]
        public int CommentFlagID { get; set; }

        [Required]
        public int CommentID { get; set; }

        [Required]
        public int FlaggerID { get; set; }

        [Required]
        public DateTime FlagDate { get; set; }

        public String FlagReason { get; set; }

        public virtual Comment Comment { get; set; }

        [ForeignKey("FlaggerID")]
        public virtual PenProfile Flagger { get; set; }
    }
}