﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("Series")]
    public partial class Series
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Series()
        {
            SeriesWritings = new HashSet<WritingSeries>();
            Subseries = new HashSet<SeriesSeries>();
            ParentSeries = new HashSet<SeriesSeries>();
            Comments = new HashSet<Comment>();
            Likes = new HashSet<Likes>();
            Followers = new HashSet<FollowerFollowing>();
            NextSeriesSeries = new HashSet<SeriesSeries>();
            PreviousSeriesSeries = new HashSet<SeriesSeries>();
        }

        [Key]
        [Required]
        public int SeriesID { get; set; }

        [Required]
        public int AccessPermissionID { get; set; }

        [Required]
        public string SeriesName { get; set; }

        public String SeriesDescription { get; set; }

        [Required]
        public bool IsComplete { get; set; }

        [Required]
        public bool IsStandAlone { get; set; }

        public virtual AccessPermission AccessPermission { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WritingSeries> SeriesWritings { get; set; }

        [ForeignKey("OverarchingSeriesID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SeriesSeries> Subseries { get; set; }

        [ForeignKey("SeriesMemberID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SeriesSeries> ParentSeries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> Comments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Likes> Likes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FollowerFollowing> Followers { get; set; }

        [ForeignKey("NextSeriesID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SeriesSeries> PreviousSeriesSeries { get; set; }

        [ForeignKey("PreviousSeriesID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SeriesSeries> NextSeriesSeries { get; set; }
    }
}