﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("Folder")]
    public partial class Folder
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Folder()
        {
            FolderSubfolders = new HashSet<FolderSubfolder>();
            Comments = new HashSet<Comment>();
            Likes = new HashSet<Likes>();
            FolderFormats = new HashSet<FolderFormat>();
            Followers = new HashSet<FollowerFollowing>();
        }

        [Key]
        [Required]
        public int FolderID { get; set; }

        [Required]
        public int AccessPermissionID { get; set; }

        [Required]
        public int OwnerID { get; set; }

        [Required]
        public string FolderName { get; set; }

        public string FolderDescription { get; set; }

        [ForeignKey("OwnerID")]
        public virtual PenProfile Owner { get; set; }

        public virtual AccessPermission AccessPermission { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FolderSubfolder> FolderSubfolders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> Comments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Likes> Likes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FolderFormat> FolderFormats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FolderGenre> FolderGenres { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FollowerFollowing> Followers { get; set; }
    }
}