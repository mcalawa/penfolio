﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("SeriesFormat")]
    public partial class SeriesFormat
    {
        [Key]
        [Required]
        public int SeriesFormatID { get; set; }

        [Required]
        public int SeriesID { get; set; }

        [Required]
        public int FormatID { get; set; }

        public virtual Series Series { get; set; }

        public virtual FormatTag FormatTag { get; set; }
    }
}