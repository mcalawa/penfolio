﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("CritiqueGiver")]
    public partial class CritiqueGiver
    {
        [Key]
        [Required]
        public int CritiqueGiverID { get; set; }

        [Required]
        public int CriticID { get; set; }

        [Required]
        public bool ForAny { get; set; }

        [Required]
        public bool ForFriends { get; set; }

        [Required]
        public bool ForMyWriters { get; set; }

        [Required]
        public bool ForProfilesFollowing { get; set; }

        [Required]
        public bool ForFoldersFollowing { get; set; }

        [Required]
        public bool ForSeriesFollowing { get; set; }

        [Required]
        public bool ForFormatFollowing { get; set; }

        [Required]
        public bool ForGenreFollowing { get; set; }

        [Required]
        public bool ForMatureWriting { get; set; }

        [ForeignKey("CriticID")]
        public virtual PenProfile Critic { get; set; }
    }
}