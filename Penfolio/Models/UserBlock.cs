﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("UserBlock")]
    public partial class UserBlock
    {
        [Key]
        [Required]
        public int UserBlockID { get; set; }

        [Required]
        public string BlockingUsername { get; set; }

        [Required]
        public string BlockedUsername { get; set; }

        public int BlockedAsProfileID { get; set; }

        [Required]
        public bool CanStillSeeOtherUser { get; set; }

        [Required]
        public bool CanSeeOtherProfilesForUser { get; set; }

        [ForeignKey("BlockingUsername")]
        public virtual PenUser BlockingUser { get; set; }

        [ForeignKey("BlockedUsername")]
        public virtual PenUser BlockedUser { get; set; }
    }
}