﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("Critique")]
    public partial class Critique
    {
        [Key]
        [Required]
        public int CritiqueID { get; set; }

        [Required]
        public int CriticID { get; set; }

        [Required]
        public int WritingID { get; set; }

        [Required]
        public DateTime CritiqueDate { get; set; }

        public DateTime? EditDate { get; set; }

        [Required]
        public byte[] EditedDocument { get; set; }

        [ForeignKey("CriticID")]
        public virtual PenProfile Critic { get; set; }

        public virtual Writing Writing { get; set; }
    }
}