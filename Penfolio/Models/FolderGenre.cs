﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("FolderGenre")]
    public partial class FolderGenre
    {
        [Key]
        [Required]
        public int FolderGenreID { get; set; }

        [Required]
        public int FolderID { get; set; }

        [Required]
        public int GenreID { get; set; }

        public virtual Folder Folder { get; set; }

        public virtual GenreTag GenreTag { get; set; }
    }
}