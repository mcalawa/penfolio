﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("CritiqueRequest")]
    public partial class CritiqueRequest
    {
        [Key]
        [Required]
        public int CritiqueRequestID { get; set; }

        [Required]
        public int WritingID { get; set; }

        [Required]
        public DateTime RequestDate { get; set; }

        public virtual Writing Writing { get; set; }
    }
}