﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("Comment")]
    public partial class Comment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Comment()
        {
            Writing = new HashSet<Writing>();
            Folder = new HashSet<Folder>();
            PenProfile = new HashSet<PenProfile>();
            Series = new HashSet<Series>();
            CommentFlags = new HashSet<CommentFlag>();
            Likes = new HashSet<Likes>();
            CommentReplies = new HashSet<CommentReply>();
            ReplyTo = new HashSet<CommentReply>();
        }

        [Key]
        [Required]
        public int CommentID { get; set; }

        [Required]
        public int CommenterID { get; set; }

        [Required]
        public DateTime AddDate { get; set; }

        public DateTime? EditDate { get; set; }

        [Required]
        public string CommentText { get; set; }

        public int? ProfileID { get; set; }

        public int? WritingID { get; set; }

        public int? FolderID { get; set; }

        public int? SeriesID { get; set; }

        [ForeignKey("CommenterID")]
        public virtual PenProfile Commenter { get; set; }

        [ForeignKey("ProfileID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PenProfile> PenProfile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Writing> Writing { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Folder> Folder { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Series> Series { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CommentFlag> CommentFlags { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Likes> Likes { get; set; }

        [ForeignKey("CommentID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CommentReply> CommentReplies { get; set; }

        [ForeignKey("ReplyID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CommentReply> ReplyTo { get; set; }
    }
}