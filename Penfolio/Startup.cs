﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Penfolio.Startup))]
namespace Penfolio
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
