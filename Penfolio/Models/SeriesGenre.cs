﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("SeriesGenre")]
    public partial class SeriesGenre
    {
        [Key]
        [Required]
        public int SeriesGenreID { get; set; }

        [Required]
        public int SeriesID { get; set; }

        [Required]
        public int GenreID { get; set; }

        public virtual Series Series { get; set; }

        public virtual GenreTag GenreTag { get; set; }
    }
}