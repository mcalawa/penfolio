﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Penfolio.Models;

namespace Penfolio.DAL
{
    public partial class PenfolioContext : DbContext
    {
        public PenfolioContext()
            : base("name=PenfolioContext")
        {
        }

        public virtual DbSet<AccessPermission> AccessPermissions { get; set; }
        public virtual DbSet<AccessRequest> AccessRequests { get; set; }
        public virtual DbSet<IndividualAccessGrant> IndividualAccessGrants { get; set; }
        public virtual DbSet<IndividualAccessRevoke> IndividualAccessRevokes { get; set; }
        public virtual DbSet<PenRole> PenRoles { get; set; }
        public virtual DbSet<PenUser> PenUsers { get; set; }
        public virtual DbSet<PenProfile> PenProfiles { get; set; }
        public virtual DbSet<Friendship> Friendships { get; set; }
        public virtual DbSet<FriendRequest> FriendRequest { get; set; }
        public virtual DbSet<UserBlock> UserBlocks { get; set; }
        public virtual DbSet<Writing> Writings { get; set; }
        public virtual DbSet<WritingProfile> WritingProfiles { get; set; }
        public virtual DbSet<Folder> Folders { get; set; }
        public virtual DbSet<WritingFolder> WritingFolders { get; set; }
        public virtual DbSet<FolderSubfolder> FolderSubfolders { get; set; }
        public virtual DbSet<Series> Series { get; set; }
        public virtual DbSet<WritingSeries> WritingSeries { get; set; }
        public virtual DbSet<SeriesSeries> SeriesSeries { get; set; }
        public virtual DbSet<FormatTag> FormatTags { get; set; }
        public virtual DbSet<AltFormatName> AltFormatNames { get; set; }
        public virtual DbSet<FormatCategory> FormatCategories { get; set; }
        public virtual DbSet<WritingFormat> WritingFormats { get; set; }
        public virtual DbSet<FolderFormat> FolderFormats { get; set; }
        public virtual DbSet<SeriesFormat> SeriesFormats { get; set; }
        public virtual DbSet<GenreTag> GenreTags { get; set; }
        public virtual DbSet<AltGenreName> AltGenreNames { get; set; }
        public virtual DbSet<GenreCategory> GenreCategories { get; set; }
        public virtual DbSet<GenreFormat> GenreFormats { get; set; }
        public virtual DbSet<WritingGenre> WritingGenres { get; set; }
        public virtual DbSet<FolderGenre> FolderGenres { get; set; }
        public virtual DbSet<SeriesGenre> SeriesGenres { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<CommentFlag> CommentFlags { get; set; }
        public virtual DbSet<CommentReply> CommentReplies { get; set; }
        public virtual DbSet<Likes> Likes { get; set; }
        public virtual DbSet<FollowerFollowing> FollowersFollowing { get; set; }
        public virtual DbSet<Critique> Critiques { get; set; }
        public virtual DbSet<CritiqueRequest> CritiqueRequests { get; set; }
        public virtual DbSet<CritiqueGiver> CritiqueGivers { get; set; }
    }
}