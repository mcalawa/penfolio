﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Penfolio.Models
{
    [Table("FriendRequest")]
    public partial class FriendRequest
    {
        [Key]
        [Required]
        public int FriendRequestID { get; set; }

        [Required]
        public int RequesterProfileID { get; set; }

        [Required]
        public int RequesteeProfileID { get; set; }

        [Required]
        public DateTime RequestDate { get; set; }

        [ForeignKey("RequesterProfileID")]
        public PenProfile RequesterProfile;

        [ForeignKey("RequesteeProfileID")]
        public PenProfile RequesteeProfile;
    }
}