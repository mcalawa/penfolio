﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("AccessRequest")]
    public partial class AccessRequest
    {
        [Key]
        [Required]
        public int AccessRequestID { get; set; }

        [Required]
        public int AccessPermissionID { get; set; }

        [Required]
        public int RequesterID { get; set; }

        [Required]
        public DateTime RequestDate { get; set; }

        public virtual AccessPermission AccessPermission { get; set; }

        [ForeignKey("RequesterID")]
        public virtual PenProfile Requester { get; set; }
    }
}