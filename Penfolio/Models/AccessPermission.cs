﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("AccessPermission")]
    public partial class AccessPermission
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AccessPermission()
        {
            Writing = new HashSet<Writing>();
            PenProfile = new HashSet<PenProfile>();
            Folder = new HashSet<Folder>();
            Series = new HashSet<Series>();
            IndividualAccessGrants = new HashSet<IndividualAccessGrant>();
            IndividualAccessRevokes = new HashSet<IndividualAccessRevoke>();
            AccessRequests = new HashSet<AccessRequest>();
        }

        [Key]
        [Required]
        public int AccessPermissionID { get; set; }

        public int? ProfileID { get; set; }

        public int? WritingID { get; set; }

        public int? FolderID { get; set; }

        public int? SeriesID { get; set; }

        [Required]
        public bool PublicAccess { get; set; }

        [Required]
        public bool FriendAccess { get; set; }

        [Required]
        public bool PublisherAccess { get; set; }

        [Required]
        public bool MinorAccess { get; set; }

        [Required]
        public bool ShowsUpInSearch { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Writing> Writing { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PenProfile> PenProfile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Folder> Folder { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Series> Series { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IndividualAccessGrant> IndividualAccessGrants { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IndividualAccessRevoke> IndividualAccessRevokes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccessRequest> AccessRequests { get; set; }
    }
}